package main

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/xanzy/go-gitlab"
)

func main() {
	git, err := gitlab.NewClient("")
	if err != nil {
		panic(err)
	}
	for _, r := range []struct {
		RepositoryName string
	}{{
		RepositoryName: "gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers",
	}, {
		RepositoryName: "gitlab-org/ci-cd/shared-runners/images/macstadium/orka",
	}, {
		RepositoryName: "gitlab-org/ci-cd/shared-runners/macos",
	}, {
		RepositoryName: "gitlab-com/gl-infra/ci-runners/deployer",
	}, {
		RepositoryName: "gitlab-com/gl-infra/ci-runners/k8s-workloads",
	}, {
		RepositoryName: "gitlab-cookbooks/cookbook-gitlab-runner",
	}, {
		RepositoryName: "gitlab-cookbooks/cookbook-wrapper-gitlab-runner",
	}, {
		RepositoryName: "gitlab-org/gitlab-runner",
	}, {
		RepositoryName: "gitlab-org/fleeting/taskscaler",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting-plugin-aws",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting-plugin-google",
	}, {
		RepositoryName: "gitlab-org/fleeting/fleeting-plugin-azure",
	}} {
		file, response, err := git.RepositoryFiles.GetFile(r.RepositoryName, ".gitlab/CODEOWNERS", &gitlab.GetFileOptions{Ref: gitlab.String("main")})
		if response.StatusCode == 404 {
			fmt.Printf("no CODEOWNERS file: %v\n", r.RepositoryName)
			continue
		}
		if err != nil {
			panic(err)
		}
		content, err := base64.StdEncoding.DecodeString(file.Content)
		if err != nil {
			panic(err)
		}
		if !strings.Contains(string(content), "* @gitlab-com/runner-maintainers") {
			fmt.Printf("CODEOWNERS missing runner-maintainers: %v\n", r.RepositoryName)
		}
	}
}
